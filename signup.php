
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Регистрация</title>
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@515;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>
    <body>
        <form action= "/signup.php" method="POST" class="content-form">
            <div class="form">

                <?php
                require "db.php";

                $data = $_POST;
                if( isset($data['do_signup']) ) :

                    $errors = array();
                    if( trim($data['login']) == '' )
                    {
                        $errors['login'] = 'Введите логин';
                    }

                    if( trim($data['email']) == '' )
                    {
                        $errors['email'] = 'Введите email';
                    }

                    if( $data['password'] == '' )
                    {
                        $errors['password'] = 'Введите пароль';
                    }

                    if( $data['password_2'] != $data['password'] )
                    {
                        $errors['password_2'] = 'Повторный пароль введён не верно!';
                    }

                    if( R::count('users', "login = ?",
                            array($data['login'])) > 0 )
                    {
                        $errors['login'] = 'Пользователь с таким логином уже существует!';

                    }

                    if( R::count('users', "email = ? ",
                            array( $data['email'])) > 0 )
                    {
                        $errors['email'] = 'Пользователь с таким email уже существует!';
                    }

                    if (empty($errors)):
                        $user = R::dispense('users');
                        $user->login = $data['login'];
                        $user->email = $data['email'];
                        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
                        R::store($user);
                        header('Location: /index.php');
                    else: ?>

                        <p><strong>Ваш логин</strong>:</p>
                        <input type="text"
                               name="login"
                               value="<?= $data['login']; ?>"
                               class=" form-control
                                                <?php if (isset($errors['login'])): ?>
                                                    is-invalid
                                                <?php else: ?>
                                                    is-valid
                                                <?php endif; ?>
                                                "
                               autofocus >
                        </p>
                            <span class="invalid-feedback" >
                                <?php if (isset($errors['login'])): ?>
                                    <?= $errors['login'] ?>
                                <?php endif; ?>
                        </span>

                        <p>
                        <p><strong>Ваш Email</strong>:</p>
                        <input type="email"
                               name="email"
                               value="<?=$data['email']; ?>"
                               class="form-control
                                            <?php if (isset($errors['email'])): ?>
                                                is-invalid
                                            <?php else: ?>
                                                is-valid
                                            <?php endif; ?>
                                        ">
                        </p>
                        <span class="invalid-feedback" >
                            <?php if (isset($errors['email'])): ?>
                                <?= $errors['email'] ?>
                            <?php endif; ?>
                        </span>

                        <p>
                        <p><strong>Ваш пароль</strong>:</p>
                        <input type="password"
                               name="password"
                               value="<?= $data['password'] ?>"
                               class="form-control
                                            <?php if (isset($errors['password'])): ?>
                                                is-invalid
                                            <?php endif; ?>
                                        ">

                        </p>
                        <span class="invalid-feedback" >
                            <?php if (isset($errors['password'])): ?>
                                <?= $errors['password'] ?>
                            <?php endif; ?>
                        </span>

                        <p>
                        <p><strong>Повторите ваш пароль</strong>:</p>
                        <input type="password"
                               name="password_2"
                               value="<?= $data['password_2']; ?>"
                               class="form-control
                                            <?php if (isset($errors['password_2'])): ?>
                                                is-invalid
                                            <?php endif; ?>
                                        ">
                        </p>

                        <span class="invalid-feedback">
                            <?php if (isset($errors['password_2'])): ?>
                                <?= $errors['password_2'] ?>
                            <?php endif; ?>
                        </span>

                        <br>
                        <br>

                        <p>
                            <button type="submit" name="do_signup">Зарегистрироваться</button>
                        </p>

                        <p>
                            <input class="btn-back" type="button" value="Назад" onclick="history.back()">
                        </p>
                    <?php endif;
                else:?>
                    <p><strong>Ваш логин</strong>:</p>
                    <input type="text" autofocus name="login" class=" form-control">
                    </p>

                    <p>
                    <p><strong>Ваш Email</strong>:</p>
                    <input type="email" name="email" class="form-control">
                    </p>

                    <p>
                    <p><strong>Ваш пароль</strong>:</p>
                    <input type="password" name="password" class="form-control">

                    </p>

                    <p>
                    <p><strong>Повторите ваш пароль</strong>:</p>
                    <input type="password" name="password_2"class="form-control">
                    </p>

                    <br>
                    <br>

                    <p>
                        <button type="submit" name="do_signup" >Зарегистрироваться</button>
                    </p>

                    <p>
                        <input class="btn-back" type="button" value="Назад" onclick="history.back()">
                    </p>
                <?php endif;?>
            </div>
        </form>
    </body>
</html>