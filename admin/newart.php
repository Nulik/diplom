<?php
require "../db.php";
$db = mysqli_connect('localhost', 'root', '', 'db_school44');

$name = $_POST['nameart'];
$text = $_POST['text'];
$date = $_POST['dat'];

$filePath = $_FILES['upload']['tmp_name'];
//$errorCode = $_FILES['image']['error'];
$name_img = md5_file($filePath);
$image_path = '/uploads/' . $name_img . '.jpg';

if (!move_uploaded_file($filePath, $_SERVER['DOCUMENT_ROOT'] . '/uploads/' . $name_img . '.jpg')) {
    die('При записи изображения на диск произошла ошибка.');
}

$news = R::dispense('news');
$news->name = $name;
$news->text = $text;
$news->date = $date;
$news->img = $image_path;
R::store($news);

header('Location: /admin/index.php');