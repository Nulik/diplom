<?php
require "../db.php";
header('Content-type: text/html; charset=utf-8');

if (! $_SESSION['admin'])
    header('Location: adminavt.php');
$db = mysqli_connect('localhost', 'root', '', 'db_school44');
mysqli_set_charset ( $db, 'utf8');

if (isset($_POST["name"])) {
    //Если это запрос на обновление, то обновляем
    if (isset($_GET['red_id'])) {
        $sql = mysqli_query($db, "UPDATE news SET 'name' = '{$_POST['name']}', 'text' = '{$_POST['text']}' WHERE 'id' ={$_GET['red_id']}");
    } else {
        //Иначе вставляем данные, подставляя их в запрос
        $sql = mysqli_query($db, "INSERT INTO news ('name', 'text') VALUES ('{$_POST['name']}', '{$_POST['text']}')");
    }

    //Если вставка прошла успешно
    if ($sql) {
        echo '<p>Успешно!</p>';
    } else {
        echo '<p>Произошла ошибка: ' . mysqli_error($db) . '</p>';
    }
}

//Если передана переменная red_id, то надо обновлять данные. Для начала достанем их из БД
if (isset($_GET['edit_id'])) {

    $new_id = $_GET['edit_id'];
    $nameart = $_POST['nameart-ed'];
    $text = $_POST['text-ed'];
    $date = $_POST['date-ed'];

    $query = mysqli_query($db, "UPDATE `news` SET `name` = '$nameart', `text` = '$text', `date` = '$date' WHERE `news`.`id` = '$new_id';");
    if ($query) {
        echo "<p>Новость успешно изменена!</p>";
    } else {

        echo '<p>Произошла ошибка: ' . mysqli_error($db) . '</p>';
    }

}

if (isset($_GET['del_id'])) { //проверяем, есть ли переменная
    $new_sql = "DELETE FROM news WHERE `id` = {$_GET['del_id']}";
    $query = mysqli_query($db, $new_sql);

    //удаляем строку из таблицы
    if ($query) {
        echo "<p>Товар удален.</p>";
    } else {
        echo '<p>Произошла ошибка: ' . mysqli_error($db) . '</p>';
    }
}

$sql = "SELECT * FROM news ORDER BY id DESC";
$news = mysqli_query($db, $sql);
?>

<!DOCTYPE html>
<html>
<head>
    <title>Страница</title>
    <meta charset="utf-8">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="card" >
                <div class="card-header"> Форма добавления новости</div>
                <div class="card-body">
                    <form action="newart.php" method="post" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="date">Дата добавления</label>
                            <div class="col-6">
                                <input id="date" class="form-control" type="datetime-local"  name="dat">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="title">Название статьи:</label>
                            <div class="col-6">
                                <input id="title" class="form-control" type="text" name="nameart">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="image">Загрузить изображение:</label>

                            <div class="col-6">
                                <input id="image" class="form-control" type="file" name="upload" accept="image/jpeg, image/png">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="message">Текст статьи:</label>
                            <div class="col-6">
                                <textarea id="message" class="form-control" name="text"></textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Добавить статью
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <br>
    <br>
    <br>
    <div class="container">


        <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
                        
        </div>

        <table  class="table table-bordered container-xl container-lg container-md container-sm ">
            <thead class="thead-dark">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Название</th>
                <th scope="col">Текст</th>
                <th scope="col">Дата</th>
                <th scope="col">Изображение</th>
                <th scope="col">Удаление</th>
                <th scope="col">Редактирование</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($result = mysqli_fetch_array($news)): ?>
                <tr>
                    <form action="?edit_id=<?=$result['id']?>" method="post">
                        <th scope='col'> <?=$result['id']?></th>
                        <td scope='col' style="max-width: 150px">
                            <label style="display: none" for="title-ed"></label>
                            <input type="text" id="title-ed" name="nameart-ed" class="form-control" value="<?=$result['name']?>">
                        </td>
                        <td scope='col'>
                            <label style="display: none" for="text-ed"></label>
                            <textarea id="text-ed" name="text-ed" class="form-control"><?=$result['text']?></textarea>
                        </td>
                        <td scope='col'>
                            <label style="display: none" for="title-ed"></label>
                            <input type="date" id="title-ed" name="date-ed" class="form-control" value="<?=$result['date']?>">
                        </td>
                        <td scope='col' style="max-width: 100px; overflow: auto"> <?=$result['img']?></td>
                        <td scope='col'><a href='?del_id=<?=$result['id']?>'>Удалить</a></td>
                        <td scope='col'><button>Изменить</button></td>
                    </form>
                </tr>
            <?php endwhile; ?>
            </tbody>
        </table>

        <div id="edit-form" class="row justify-content-center"   style="display: none">
            <div class="card" style="width: 80%">
                <div class="card-header"> Изменить новость</div>
                <div class="card-body">
                    <form action="?edit-id" method="post">
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="ed-title">Название статьи:</label>
                            <div class="col-md-6">
                                <input id="ed-title" type="text"  class="form-control"  name="nameart" value="<?= isset($_GET['edit_id']) ? $product['name'] : ''; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="ed-message">Текст статьи:</label>
                            <div class="col-md-6">
                                <textarea id="ed-message" name="text" class="form-control"  value="<?= isset($_GET['red_id']) ? $product['text'] : ''; ?>"></textarea>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Изменить статью
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <br>
    <br>
    <br>
<div class="container">
    <div class="card" >
        <form action="gallery_admin.php" method="post" enctype="multipart/form-data">
            <label class="col-md-8 col-form-label text-md-right" >Добавление изображения  в галерею!</label>
            <br>
            <br>
            <div class="form-group row">
                <label class="col-md-4 col-form-label text-md-right" for="image">Загрузить изображение:</label>
                <div class="col-6">
                    <input id="image" class="form-control" type="file" name="upload" accept="image/jpeg, image/png">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Добавить изображение
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
<script>

</script>
