<?php
require "db.php";

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Школа № 44</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@515;600&display=swap" rel="stylesheet">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Login style  -->
    <link rel="stylesheet" href="css/login_css.css">
    <!-- Icon -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="fh5co-loader"></div>
<div id="page">
    <?php include_once 'includes/block_modal_form.php' ?>
    <nav class="fh5co-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><a href="index.php"><i class="fas fa-bookmark"></i></a></div>
                    </div>
                    <div id="logo" class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="dws-li"><a href="index.php"><i class="fas fa-home" aria-hidden="true"></i>Главная</a></li>
                            <li class="dws-li"><a href="about.php"><i class="fas fa-user" aria-hidden="true"></i>О школе</a></li>
                            <li class="dws-li has-dropdown">
                                <a href="student.php"><i class="fas fa-graduation-cap" aria-hidden="true"></i>Ученикам</a>
                                <ul class="dropdown">
                                    <li class="drp-li"><a href="security.php">Безопастность</a></li>
                                    <li class="drp-li"><a href="mediation-service.php">Служба медиации</a></li>
                                    <li class="drp-li"><a href="internet-security.php">Безопасность в сети</a></li>
                                    <li class="drp-li"><a href="gallery.php">Галерея школы</a></li>
                                    <li class="drp-li"><a href="student.php #timetable">Расписание</a></li>
                                </ul>
                            </li>
                            <li class="dws-li active"><a href="contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Контакты</a></li>
                            <?php if( isset($_SESSION['logged_user']) ) : ?>
                                Привет, <?php echo $_SESSION['logged_user']->login; ?>
                                <li class="dws-li"><a href="/logout.php"><i class="fas fas-door-open" aria-hidden="true"></i>Выйти</a></li>
                            <?php else : ?>
                                <li class="btn-cta"><a href="#login-modal" data-toggle="modal"><span>Войти</span></a></li>
                                <li class="btn-cta"><a href="/signup.php"><span>Зарегистрироваться</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <aside id="fh5co-hero">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/img_bg_new_title.jpg);">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1 class="heading-section">Свяжитесь с нами</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background-image: url(images/img_bg_new_title.jpg);">
                    <div class="overlay"></div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 text-center slider-text">
                                <div class="slider-text-inner">
                                    <h1>Хорошие учителя создают хороших учеников</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="fh5co-contact">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-push-1 animate-box">

                    <div class="fh5co-contact-info">
                        <h3>Наши контакты</h3>
                        <ul>
                            <li class="icon-1"><a>Г. Курган, ул. Бородина 41, <br> МБОУ "Средняя общеобразовательная <br>школа № 44"</a></li>
                            <li class="icon-2"><a>8 (3522) 25-03-49, (3522) 41-78-04</a></li>
                            <li class="icon-3"><a class="p" href="mailto:shkola44.kurgan@mail.ru" >shkola44.kurgan@mail.ru</a></li>
                        </ul>
                    </div >

                </div>

                <div class="col-md-6 animate-box">
                    <?php
                    if (isset($_GET['send_mess'])) :
                        if ($_SERVER['REQUEST_METHOD'] == 'POST') :

                            $data = $_POST;
                            $errors = array();

                            if ($data['nameart'] === '') {
                                $errors['nameart'] = 'Вы не написали ваше имя';
                            }

                            if ($data['email'] === '') {
                                $errors['email'] = 'Вы не указали вашу почту';
                            }

                            if ($data['subject'] === '') {
                                $errors['subject'] = 'Вы не указали тему сообщения';
                            }

                            if ($data['message'] === '') {
                                $errors['message'] = 'Вы не написали сообщение';
                            }

                            if (empty($errors)) :
                                    $feedback = R::dispense('feedback');
                                    $feedback->nameart = $data['nameart'];
                                    $feedback->email = $data['email'];
                                    $feedback->subject = $data['subject'];
                                    $feedback->message = $data['message'];
                                    R::store($feedback);
                                    ?>
                                <h3>Обратная связь</h3>
                                <p>Сообщение отправлено</p>
                                <form action="?send_mess=1" method="post">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input type="text" name="nameart" class="form-control one" placeholder="Ваше ФИО">

                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <!-- <label for="email">Email</label> -->
                                            <input type="text" name="email" class="form-control" placeholder="Ваш email адрес">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <!-- <label for="subject">Subject</label> -->
                                            <input type="text" name="subject" class="form-control" placeholder="Ваша тема сообщения">
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <!-- <label for="message">Message</label> -->
                                            <textarea name="message"  cols="30" rows="10" class="form-control" placeholder="Ваше сообщение"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Отправить сообщение" class="btn btn-primary">
                                    </div>
                                </form>

                            <?php
                            else : ?>
                                <h3>Обратная связь</h3>
                                <form action="?send_mess=1" method="post">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input type="text"
                                                   name="nameart"
                                                   value="<?= $data['nameart'] ?>"
                                                   class="form-control
                                                                <?php if (isset($errors['nameart'])): ?>
                                                                    is-invalid
                                                                <?php endif; ?>
                                                                "
                                                   placeholder="Ваше ФИО">

                                            <span class="invalid-feedback" >
                                                    <?php if (isset($errors['nameart'])): ?>
                                                        <?= $errors['nameart'] ?>
                                                    <?php endif; ?>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <!-- <label for="email">Email</label> -->
                                            <input type="text"
                                                   name="email"
                                                   value="<?= $data['email'] ?>"
                                                   class="form-control
                                                                <?php if (isset($errors['email'])): ?>
                                                                    is-invalid
                                                                <?php endif; ?>
                                                                "
                                                   placeholder="Ваш email адрес">

                                            <span class="invalid-feedback" >
                                                    <?php if (isset($errors['email'])): ?>
                                                        <?= $errors['email'] ?>
                                                    <?php endif; ?>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <!-- <label for="subject">Subject</label> -->
                                            <input type="text"
                                                   name="subject"
                                                   value="<?= $data['subject'] ?>"
                                                   class="form-control
                                                                <?php if (isset($errors['subject'])): ?>
                                                                    is-invalid
                                                                <?php endif; ?>
                                                                "
                                                   placeholder="Ваша тема сообщения">

                                            <span class="invalid-feedback" >
                                                    <?php if (isset($errors['subject'])): ?>
                                                        <?= $errors['subject'] ?>
                                                    <?php endif; ?>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <!-- <label for="message">Message</label> -->
                                            <textarea name="message"
                                                      cols="30"
                                                      rows="10"
                                                      value="<?= $data['message'] ?>"
                                                      class="form-control
                                                                <?php if (isset($errors['message'])): ?>
                                                                    is-invalid
                                                                <?php endif; ?>
                                                                "
                                                      placeholder="Ваше сообщение"></textarea>

                                            <span class="invalid-feedback" >
                                                    <?php if (isset($errors['message'])): ?>
                                                        <?= $errors['message'] ?>
                                                    <?php endif; ?>
                                                </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" value="Отправить сообщение" class="btn btn-primary">
                                    </div>
                                </form>

                            <?php
                            endif;
                        else:?>
                            <h3>Обратная связь</h3>
                            <form action="?send_mess=1" method="post">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <input type="text" name="nameart" class="form-control one" placeholder="Ваше ФИО">

                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <!-- <label for="email">Email</label> -->
                                        <input type="text" name="email" class="form-control" placeholder="Ваш email адрес">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <!-- <label for="subject">Subject</label> -->
                                        <input type="text" name="subject" class="form-control" placeholder="Ваша тема сообщения">
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <!-- <label for="message">Message</label> -->
                                        <textarea name="message"  cols="30" rows="10" class="form-control" placeholder="Ваше сообщение"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Отправить сообщение" class="btn btn-primary">
                                </div>
                            </form>
                        <?php endif;?>
                    <?php else:?>
                        <h3>Обратная связь</h3>
                        <form action="?send_mess=1" method="post">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <input type="text" name="nameart" class="form-control one" placeholder="Ваше ФИО">

                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <!-- <label for="email">Email</label> -->
                                    <input type="text" name="email" class="form-control" placeholder="Ваш email адрес">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <!-- <label for="subject">Subject</label> -->
                                    <input type="text" name="subject" class="form-control" placeholder="Ваша тема сообщения">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <!-- <label for="message">Message</label> -->
                                    <textarea name="message"  cols="30" rows="10" class="form-control" placeholder="Ваше сообщение"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Отправить сообщение" class="btn btn-primary">
                            </div>
                        </form>
                    <?php
                    endif;?>
                </div>
            </div>

        </div>
    </div>
    <div id="fh5co-map" class="fh5co-bg-section">
        <div class="row text-center">
            <div class="col-md-12">
                <h2><span>Наша школа на карте</span></h2>
            </div>
        </div>
    </div>
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A34c9940218824fed084428560e632d4cb0921cc253b364d37042038def8ec88b&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
    <div id="fh5co-gallery" class="fh5co-bg-section"></div>

    <?php include_once 'includes/block_footer.php' ?>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></a>
</div>

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="js/jquery.stellar.min.js"></script>
<!-- Carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- Flexslider -->
<script src="js/jquery.flexslider-min.js"></script>
<!-- countTo -->
<script src="js/jquery.countTo.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/magnific-popup-options.js"></script>
<!-- Count Down -->
<script src="js/simplyCountdown.js"></script>
<!-- Main -->
<script src="js/main.js"></script>
<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
    $('#login-modal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
</script>
</body>
</html>