<?php
require "db.php";
?>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Школа № 44</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@515;600&display=swap" rel="stylesheet">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Login style  -->
    <link rel="stylesheet" href="css/login_css.css">
    <!-- Icon -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
	<body>

    <div class="fh5co-loader"></div>
    <div id="page">
    <?php include_once 'includes/block_modal_form.php' ?>
    <nav class="fh5co-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><a href="index.php"><i class="fas fa-bookmark"></i></a></div>
                    </div>
                    <div id="logo" class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="dws-li"><a href="index.php"><i class="fas fa-home" aria-hidden="true"></i>Главная</a></li>
                            <li class="dws-li"><a href="about.php"><i class="fas fa-user" aria-hidden="true"></i>О школе</a></li>
                            <li class="dws-li has-dropdown active">
                                <a href="student.php"><i class="fas fa-graduation-cap" aria-hidden="true"></i>Ученикам</a>
                                <ul class="dropdown">
                                    <li class="drp-li"><a href="security.php">Безопасность</a></li>
                                    <li class="drp-li"><a href="mediation-service.php">Служба медиации</a></li>
                                    <li class="drp-li"><a href="internet-security.php">Безопасность в сети</a></li>
                                    <li class="drp-li"><a href="gallery.php">Галерея школы</a></li>
                                    <li class="drp-li"><a href="student.php #timetable">Расписание</a></li>
                                </ul>
                            </li>
                            <li class="dws-li"><a href="contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Контакты</a></li>
                            <?php if( isset($_SESSION['logged_user']) ) : ?>
                                Привет, <?php echo $_SESSION['logged_user']->login; ?>
                                <li class="dws-li"><a href="/logout.php"><i class="fas fa-door-open" aria-hidden="true"></i>Выйти</a></li>
                            <?php else : ?>
                                <li class="btn-cta"><a href="#login-modal" data-toggle="modal"><span>Войти</span></a></li>
                                <li class="btn-cta"><a href="/signup.php"><span>Зарегистрироваться</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
	<aside id="fh5co-hero">
		<div class="flexslider">
			<ul class="slides">
		   	<li style="background-image: url(images/img_bg_new_title.jpg);">
		   		<div class="overlay"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1 class="heading-section">Информация для учеников</h1>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
            </li>
            <li style="background-image: url(images/img_bg_new_title.jpg);">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 text-center slider-text">
                            <div class="slider-text-inner">
                                <h1>Хорошие учителя создают хороших учеников</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
		  	</ul>
	  	</div>
	</aside>
	
	<div id="fh5co-staff">
		<div class="container">
            <div class="row animate-box">
                <div class="col-md-6 col-md-offset-3 col-sm-12 text-center fh5co-heading">
                    <h2>Важно знать ученикам</h2>
                    <p>Страеница предназначена для ознакомления учеников со следующими разделами</p>
                </div>
            </div>

			<div class="row">
				<div class="col-md-4 col-sm-6 text-center">
					<div class="staff">
						<div class="staff-img" style="background-image: url(images/img-staff-1.jpg);">
							<ul class="fh5co-social">
								<li><a href="security.php"><i class="fas fa-info-circle"></i>Подробнее...</i></a></li>
							</ul>
						</div>
						<p>Пожарная безопасность</p>
					</div>
				</div>
                <div class="col-md-4 col-sm-6 animate-box text-center">
                    <div class="staff">
                        <div class="staff-img" style="background-image: url(images/img-staff-2.jpg);">
                            <ul class="fh5co-social">
                                <li><a href="internet-security.php"><i class="fas fa-info-circle"></i>Подробнее...</i></a></li>
                            </ul>
                        </div>
                        <p>Интернет безопасность</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 animate-box text-center">
                    <div class="staff-1">
                        <div id="timetable" style="background-image: url(images/staff-2.jpg);"><a href="images/img-staff-6.jpg" class="photo img-responsive"><img src="images/img-staff-6-1.jpg" alt=""></a></div>
                        <p class="one">Расписание звонков ( 1 смена)</p>
                    </div>
                </div>

				<div class="col-xs-12 col-sm-12 col-md-8 animate-box text-center">
                    <div class="staff">
                        <div class="staff-img" style="background-image: url(images/img-staff-14.jpg);">
                            <ul class="fh5co-social">
                                <li><a href="mediation-service.php"><i class="fas fa-info-circle"></i>Подробнее...</i></a></li>
                            </ul>
                        </div>
                        <p>Школьная служба медиации</p>
                    </div>
				</div>
                <div class=" col-md-4 col-sm-6 animate-box text-center">
                    <div class="staff-1">
                        <div style="background-image: url(images/staff-2.jpg);"><a href="images/img-staff-7.jpg" class="photo img-responsive"><img src="images/img-staff-7-2.jpg" alt=""></a>
                        </div>
                        <p class="one">Расписание звонков ( 2 смена)</p>
                    </div>
                </div>

			</div>
		</div>
	</div>

    <?php include_once 'includes/block_timer.php' ?>

    <?php include_once 'includes/block_footer.php' ?>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Count Down -->
	<script src="js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>
	<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
    $('#login-modal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
	</script>
	</body>
</html>