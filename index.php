<?php
require "db.php";
$db = mysqli_connect('localhost', 'root', '', 'db_school44');
mysqli_set_charset ( $db, 'utf8');
function get_news()
{
    global $db;
    $query = "SELECT * FROM news ORDER BY date DESC limit 10";
    $result = mysqli_query($db, $query);
    $news = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $news;
}
?>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Школа № 44</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Comfortaa:wght@515;600&display=swap" rel="stylesheet">
    <!-- Animate.css -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="css/bootstrap.css">
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Login style  -->
    <link rel="stylesheet" href="css/login_css.css">
    <!-- Icon -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- Modernizr JS -->
    <script src="js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
	<body>
	<div class="fh5co-loader"></div>
	<div id="page">

    <nav class="fh5co-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="fh5co-logo"><a href="index.php"><i class="fas fa-bookmark"></i></a></div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul>
                            <li class="dws-li active"><a href="index.php"><i class="fas fa-home" aria-hidden="true"></i>Главная</a></li>
                            <li class="dws-li"><a href="about.php"><i class="fas fa-user" aria-hidden="true"></i>О школе</a></li>
                            <li class="dws-li has-dropdown">
                                <a href="student.php"><i class="fas fa-graduation-cap"></i>Ученикам</a>
                                <ul class="dropdown">
                                    <li class="drp-li"><a href="security.php">Безопасность</a></li>
                                    <li class="drp-li"><a href="mediation-service.php">Служба медиации</a></li>
                                    <li class="drp-li"><a href="internet-security.php">Безопасность в сети</a></li>
                                    <li class="drp-li"><a href="gallery.php">Галерея школы</a></li>
                                    <li class="drp-li"><a href="student.php #timetable">Расписание</a></li>
                                </ul>
                            </li>
                            <li class="dws-li"><a href="contact.php"><i class="fas fa-address-book" aria-hidden="true"></i>Контакты</a></li>
                            <?php if( isset($_SESSION['logged_user']) ) : ?>
                                Привет, <?php echo $_SESSION['logged_user']->login; ?>
                                <li class="dws-li"><a href="/logout.php"><i class="fas fa-door-open"></i>Выйти</a></li>
                            <?php else : ?>
                                <li class="btn-cta"><a href="#login-modal" data-toggle="modal"><span>Войти</span></a></li>
                                <li class="btn-cta"><a href="/signup.php"><span>Зарегистрироваться</span></a></li>
                                <li class="btn-cta"><a href="/admin/avtadministrator.php" data-toggle="modal"><span>Админ</span></a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
        <?php include_once 'includes/block_modal_form.php' ?>

	<aside id="fh5co-hero">
		<div class="flexslider">
			<ul class="slides">
		   	<li style="background-image: url(images/img_bg_new_title.jpg);">
		   		<div class="overlay"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1>"Средняя общеобразовательная школа №44"</h1>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url(images/img_bg_new_title.jpg);">
		   		<div class="overlay"></div>
		   		<div class="container">
		   			<div class="row">
			   			<div class="col-md-8 col-md-offset-2 text-center slider-text">
			   				<div class="slider-text-inner">
			   					<h1>Хорошие учителя создают хороших учеников</h1>
			   				</div>
			   			</div>
			   		</div>
		   		</div>
		   	</li>
		  	</ul>
	  	</div>
	</aside>

	<div id="fh5co-course">
        <div class="container">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>Новости школы</h2>
					<p>Самые последние новости школы №44</p>
				</div>
			</div>
                <?php
                    $news = get_news();
                ?>
                <?php foreach ($news as $new):
                ?>
                <div class="row">
                    <div class="col-md-4 col-xs-9">
                        <div class="thumbnail">
                            <img src="<?=$new ['img']?> " class="img-rounded" alt="">
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 animate-box">
                        <div class="staff-2 ">
                            <h3><?=$new ['name']?></h3>
                            <hr>
                            <p><?=$new ['text']?></p>
                            <hr>
                            <ul class="list-inline">
                                <li><i class="glyphicon glyphicon-calendar"></i> <?=$new ['date']?> </li>
                            </ul>
                        </div>
                    </div>

                </div>
                <?php endforeach; ?>
            </div>
        </div>

		<div id="fh5co-counter" class="fh5co-counters" style="background-image: url(images/img_bg_new_3.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div class="row">
							<div class="col-md-3 col-sm-6 text-center animate-box">
								<span class="icon"><i class="fas fa-users"></i></span>
								<span class="fh5co-counter js-counter" data-from="0" data-to="3297" data-speed="5000" data-refresh-interval="50"></span>
								<span class="fh5co-counter-label">Foreign Followers</span>
							</div>
							<div class="col-md-3 col-sm-6 text-center animate-box">
								<span class="icon"><i class="icon-study"></i></span>
								<span class="fh5co-counter js-counter" data-from="0" data-to="3700" data-speed="5000" data-refresh-interval="50"></span>
								<span class="fh5co-counter-label">Students Enrolled</span>
							</div>
							<div class="col-md-3 col-sm-6 text-center animate-box">
								<span class="icon"><i class="icon-bulb"></i></span>
								<span class="fh5co-counter js-counter" data-from="0" data-to="5034" data-speed="5000" data-refresh-interval="50"></span>
								<span class="fh5co-counter-label">Classes Complete</span>
							</div>
							<div class="col-md-3 col-sm-6 text-center animate-box">
								<span class="icon"><i class="icon-head"></i></span>
								<span class="fh5co-counter js-counter" data-from="0" data-to="1080" data-speed="5000" data-refresh-interval="50"></span>
								<span class="fh5co-counter-label">Certified Teachers</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div id="fh5co-state-links" class="fh5co-bg-section">

            <div class="row text-center">
                <div class="col-md-12">
                    <h2><span>Государственные ссылки</span></h2>
                </div>
            </div>
            <div class="row library">
                <div class="col-md-3 col-xs-10 col-padded">
                    <a href="http://don.kurganobl.ru/index.php?option=com_content&view=section&id=45&Itemid=237" target="_blank" class="state-links" style="background-image: url(images/img_new_1.jpg);"></a>
                </div>
                <div class="col-md-3 col-xs-10 col-padded">
                    <a href="http://fcior.edu.ru/" target="_blank" class="state-links" style="background-image: url(images/img_new_2.jpg);"></a>
                </div>

                <div class="col-md-3 col-xs-10 col-padded">
                    <a href="https://www.kurgan-city.ru/" target="_blank" class="state-links" style="background-image: url(images/img_new_3.jpg);"></a>
                </div>
                <div class="col-md-3 col-xs-10 col-padded">
                    <a href="http://window.edu.ru/resource/981/47981" target="_blank" class="state-links" style="background-image: url(images/img_new_4.jpg);"></a>
                </div>
            </div>
        </div>

    <?php include_once 'includes/block_footer.php' ?>
	</div>


	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="fas fa-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- Flexslider -->
	<script src="js/jquery.flexslider-min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Count Down -->
	<script src="js/simplyCountdown.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>
	<script>
    var d = new Date(new Date().getTime() + 1000 * 120 * 120 * 2000);

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });

    $('#login-modal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })

	</script>
	</body>
</html>