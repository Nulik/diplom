<?php
require "db.php";

$data = $_POST;
if( isset($data['do_login']))
{
    $errors = array();
    $user = R::findOne('users', 'login = ?', array($data['login']));
    if( $user )
    {
        // логин существует
        if( password_verify($data['password'], $user->password)
        ) {
            //все хорошо логиним пользователя
            $_SESSION['logged_user'] = $user;
            $success = [
                'code' => 200,
                'message' => 'Успешный вход',
            ];
            header('Location: /');
        } else
        {
          $errors['password'] = 'Неверно введён пароль!';
        }
    }  else
    {
        $errors['login'] = 'Пользователь с таким логином не найден!';
    }

    if( ! empty($errors) )
    {
        return json_encode($errors);
    }
}
?>