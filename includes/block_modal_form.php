<?php
if (isset($_GET['login'])):
    $data = $_POST;
    if( isset($data['do_login'])) :
        $errors = array();
        $user = R::findOne('users', 'login = ?', array($data['login']));
        if( $user ) {
            // логин существует
            if( password_verify($data['password'], $user->password)) {
                //все хорошо логиним пользователя
                $_SESSION['logged_user'] = $user;
                header('Location: /');
            } else {
                $errors['password'] = 'Неверно введён пароль!';
            }
        }  else {
            $errors['login'] = 'Пользователь с таким логином не найден!';
        }

        if( !empty($errors) ) :?>
            <div class="modal fadeInDownBig " id="login-modal">
                <div class="modal-dialog ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Авторизация</i></h4>
                        </div>
                        <form action="?login=1" id="login-form" method="POST" class="content-form">
                            <div class="modal-body">
                                <div class="form">
                                    <p><label for="myInput">Логин</label>:</p>
                                    <input id="myInput"
                                           type="text"
                                           class="form-control
                                                        <?php if (isset($errors['login'])): ?>
                                                            is-invalid
                                                        <?php else: ?>
                                                            is-valid
                                                        <?php endif; ?>
                                                   "
                                           name="login"
                                           value="<?php echo @$data['login']; ?>">
                                    <span class="invalid-feedback">
                                                <?php if (isset($errors['login'])): ?>
                                                    <?= $errors['login'] ?>
                                                <?php endif; ?>
                                            </span>
                                    <p><label for="password">Пароль</label>:</p>
                                    <input id="password"
                                           type="password"
                                           name="password"
                                           class="form-control
                                                        <?php if (isset($errors['password'])): ?>
                                                            is-invalid
                                                        <?php endif; ?>
                                                   "
                                           value="<?php echo @$data['password']; ?>">

                                    <span class="invalid-feedback">
                                                <?php if (isset($errors['password'])): ?>
                                                    <?= $errors['password'] ?>
                                                <?php endif; ?>
                                            </span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button  class="btn-1" type="submit" name="do_login">Войти</button>
                                <button class="btn-2" type="button" data-dismiss="modal">Закрыть</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script>
                window.onload = function () {alert('Ошибка авторизации, попробуйте снова!');};

            </script>
        <?php endif;
    endif;
    ?>
<?php else: ?>
    <div class="modal fadeInDownBig " id="login-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Авторизация<i class="fas fa-sign-in-alt"></i></h4>
                </div>
                <form action="?login=1" id="login-form" method="POST" class="content-form">
                    <div class="modal-body">
                        <div class="form">
                            <p><label for="myInput">Логин</label>:</p>
                            <input id="myInput" type="text" name="login" value="<?php echo @$data['login']; ?>">
                            <p><label for="password">Пароль</label>:</p>
                            <input id="password" type="password" name="password" value="<?php echo @$data['email']; ?>">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button  class="btn-1" type="submit" name="do_login">Войти</button>
                        <button class="btn-2" type="button" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php endif; ?>