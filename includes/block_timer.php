<div id="fh5co-timer" style="background-image: url(../images/img_bg_2.jpg);">
    <div class="overlay"></div>
    <div class="row ">
        <div class="col-md-8 col-md-offset-2 animate-box">
            <div class="date-counter text-center">
                <h2>До начала учебного года осталось:</h2>
                <div class="simply-countdown simply-countdown-one"></div>
                <p><strong>Учиться, учиться и еще раз учиться!</strong></p>
            </div>
        </div>
    </div>
</div>