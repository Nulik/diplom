<footer id="fh5co-footer" role="contentinfo" style="background-image: url(../images/img_bg_new_3.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="row row-pb-md">
            <div class="col-md-4 fh5co-widget">
                <h3>О нашей школе</h3>
                <p class="text">Муниципальное бюджетное общеобразовательное учреждение города Кургана «Средняя общеобразовательная школа № 44»</p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
                <h3>Навигация по сайту</h3>
                <ul class="fh5co-footer-links">
                    <li><a href="../index.php">Главная</a></li>
                    <li><a href="../about.php">О школе</a></li>
                    <li><a href="../gallery.php">Галерея</a></li>
                    <li><a href="../student.php">Ученикам</a></li>
                    <li><a href="../contact.php">Контакты</a></li>
                </ul>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-6 col-md-push-1 fh5co-widget">
                <h3>Информация для учеников</h3>
                <ul class="fh5co-footer-links">
                    <li><a href="../student.php #timetable">Расписание</a></li>
                    <li><a href="../security.php">Безопастность</a></li>
                    <li><a href="../internet-security.php">Безопасность в сети</a></li>
                    <li><a href="../mediation-service.php">Служба медиации</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-xl-12 row copyright">

        <div class="col-md-12  text-center">
            <small class="block">&copy; МБОУ "Средняя общеобразовательная школа №44" 2020 | г. Курган</small>
        </div>
    </div>

</footer>